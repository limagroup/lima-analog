import numpy
import time
import tabulate
import argparse
#import matplotlib.pyplot as plt
import pylab as plt

class LogIndex:
    POINT = 0
    EPOCH = 1
    COMP_TIME = 2
    COMP_RATE = 3
    COMP_RATIO = 4
    WRITE_TIME = 5
    WRITE_RATE = 6
    TOTAL_TIME = 7

class StatLogFile:
    def __init__(self, filename):
        self.filename = filename

        # --- read log stat file
        data = list()
        frame_per_file = 1
        file = open(filename, "r")
        for line in file.readlines():
            if not len(line):
                continue
            if line.startswith("#"):
                if line.find("framesPerFile")>0:
                    frame_per_file = line.split("framesPerFile=")[1].split(",")[0]
                    frame_per_file = int(frame_per_file)
                continue
            values = line.split()
            if len(values) == 8:
                data.append(numpy.array(values, numpy.float64))

        if not len(data):
            raise RuntimeError(f"No stats found in {filename}")

        self.frame_data = numpy.array(data)
        self.frame_per_file = frame_per_file

        # --- computes per file data
        if frame_per_file > 1:
            file_data = list()
            for idx in range(0, self.frame_data.shape[0], frame_per_file):
                calc_data = self.frame_data[idx:idx+200,:].sum(axis=0)
                calc_data[LogIndex.COMP_RATIO] /= frame_per_file
                calc_data[LogIndex.COMP_RATE] /= frame_per_file
                calc_data[LogIndex.COMP_TIME] /= frame_per_file
                calc_data[LogIndex.WRITE_RATE] /= frame_per_file
                file_data.append(calc_data)
            self.file_data = numpy.array(file_data, numpy.float64)
        else:
            self.file_data = data

        # --- file dates
        self.time_start = time.localtime(self.frame_data[0,LogIndex.EPOCH])
        self.time_end = time.localtime(self.frame_data[-1,LogIndex.EPOCH])
        self.date_string = "from {0} to {1}".format(
              time.strftime("%Y/%m/%d-%H:%M:%S", self.time_start),
              time.strftime("%Y/%m/%d-%H:%M:%S", self.time_end)
              )

    def print_stat(self):
        stats = list()
        comp_time = self.frame_data[:,LogIndex.COMP_TIME]
        stats.append(["Compression Time (msec)", comp_time.min(), comp_time.max(), comp_time.mean()])
        comp_ratio = self.frame_data[:,LogIndex.COMP_RATIO]
        stats.append(["Compression Ratio", comp_ratio.min(), comp_ratio.max(), comp_ratio.mean()])
        write_time = self.frame_data[:,LogIndex.WRITE_TIME]
        stats.append(["Write Time (msec)", write_time.min(), write_time.max(), write_time.mean()])
        total_time = self.frame_data[:,LogIndex.TOTAL_TIME]
        stats.append(["Total Time (msec)", total_time.min(), total_time.max(), total_time.mean()])

        file_time = self.file_data[:,LogIndex.WRITE_TIME]
        stats.append(["Write Time Per File (msec)", file_time.min(), file_time.max(), file_time.mean()])

        print(f"\nLOGFILE {self.filename}:")
        print(self.date_string)
        print()
        heads = [ "", "MIN", "MAX", "MEAN" ]
        print(tabulate.tabulate(stats, headers=heads))
        print()

    def plot_per_frame(self):
        print("Plotting stats per frame ... (close plot to continue)")
        fig, axs = plt.subplots(4, sharex=True)
        fig.suptitle(f"{self.filename}\n{self.date_string}")

        self.add_subplot(axs[0], self.frame_data[:,LogIndex.COMP_RATIO], "Compression Ratio Per Frame", "black", "ratio")
        self.add_subplot(axs[1], self.frame_data[:,LogIndex.COMP_TIME], "Compression Time Per Frame", "blue", "msec")
        self.add_subplot(axs[2], self.frame_data[:,LogIndex.WRITE_TIME], "Write Time Per Frame", "green", "msec")
        self.add_subplot(axs[3], self.frame_data[:,LogIndex.TOTAL_TIME], "Total Time Per Frame", "red", "msec")

        for ax in axs:
            ax.set(xlabel="point number")
            ax.label_outer()

        plt.show()

    def add_subplot(self, ax, stat, title, color, ylabel):
        ax.plot(stat, color=color)
        ax.set_title(title)
        ax.set(ylabel=ylabel)
        ax.set_ylim(bottom=0.0)
        text = "MIN {0:.1f} MAX {1:.1f} MEAN {2:.1f}".format(stat.min(), stat.max(), stat.mean())
        ax.text(ax.get_xlim()[1]*1/4, ax.get_ylim()[1]*7/8, text)

    def plot_per_file(self):
        print("Plotting stats per file ... (close plot to continue)")
        fig, axs = plt.subplots(3, sharex=True)
        fig.suptitle(f"{self.filename}\n{self.date_string}")

        self.add_subplot(axs[0], self.file_data[:,LogIndex.COMP_RATIO], "Average Compression Ratio Per File", "black", "ratio")
        self.add_subplot(axs[1], self.file_data[:,LogIndex.COMP_TIME], "Average Compression Time Per File", "blue", "msec")
        self.add_subplot(axs[2], self.file_data[:,LogIndex.WRITE_TIME], "Write Time Per File", "green", "msec")

        for ax in axs:
            ax.set(xlabel="file number")
            ax.label_outer()

        plt.show()


def plot_per_group(stat_list, gsize):
    stats = list()
   
    plt.figure()
    plt.suptitle("Write Time Per File [msec]")

    while len(stat_list):
        for idx in range(gsize):
            try:
                stat = stat_list.pop(0)
                data = stat.file_data[:,LogIndex.WRITE_TIME]
                stats.append([stat.filename, data.min(), data.max(), data.mean()])
                plt.plot(data, label=stat.filename)
            except:
                pass

        plt.legend(loc=0)
        plt.show()

    print("\n=== Write Time Per File [msec] ===")
    heads = [ "", "MIN", "MAX", "MEAN" ]
    print(tabulate.tabulate(stats, headers=heads))
    print()


def main():
    parser = argparse.ArgumentParser(description= "Lima saving statistics log analyzer")
    parser.add_argument('filename', type=str, nargs="+", 
                        help="lima saving statistics log filename")
    parser.add_argument('--print-stat', action='store_true', dest="print_stat",
                        help="print statictics min/max/mean")
    parser.add_argument('--plot-per-file', action='store_true', dest="plot_per_file",
                        help="plot statistics per lima file")
    parser.add_argument('--plot-per-frame', action='store_true', dest="plot_per_frame",
                        help="plot statistics per frame")
    parser.add_argument('--plot-per-group', action='store_true', dest="plot_per_group",
                        help="plot write time per group of log files")
    parser.add_argument('--group-by', type=int, default=10, dest="group_by",
                        help="number of files per plot group")
    options = parser.parse_args()


    if options.plot_per_group:
        stats = list()
        for filename in options.filename:
            try:
                print(f"Reading {filename} ...")
                stat = StatLogFile(filename)
                stats.append(stat)
            except:
                print(f"FAILED to read {filename}")
        if len(stats):
            plot_per_group(stats, options.group_by)
    else:

        if not any([options.print_stat, options.plot_per_frame, options.plot_per_file]):
            options.print_stat = True
            options.plot_per_frame = True
            options.plot_per_file = True

        for filename in options.filename:
            stat = StatLogFile(filename)
            if options.print_stat:
                stat.print_stat()
            if options.plot_per_file:
                stat.plot_per_file()
            if options.plot_per_frame:
                stat.plot_per_frame()

if __name__=="__main__":
    main()

