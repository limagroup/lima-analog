from distutils.core import setup

setup(name='lima_analog',
      version='0.1.0',
      description='Lima stat log file analyzer',
      author='Emmanuel Papillon',
      author_email='papillon@esrf.fr',
      url='https://gitlab.esrf.fr/limagroup/lima-analog',
      py_modules=['lima_analog'],
      entry_points={'console_scripts': ['lima_analog=lima_analog:main'] }
     )
