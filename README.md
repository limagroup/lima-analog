**INSTALLATION**

in a bliss conda environment, use:

`pip install .` or `pip install -e .`

then run the script:

`lima_analog`

**DESCRIPTION**

Produce matplotlib plots of lima saving statistics files.
To generate lima statistics log files, on the lima device, you have to set following attributes:

- *saving_statistics_log_enable* = True
- *[](url)saving_statistics_history* = npoints of your acquisition

Saving statistics plots for one log file can be shown by frame or by lima saved file (using saving_frame_per_file saved in log file).

For a quick view on a group of file, the option *--plot-per-group* can be used to display together write time of several log stat files (group by 5 or by *--group-by* files).

**SCRIPT USAGE**
```
usage: lima_analog [-h] [--print-stat] [--plot-per-file] [--plot-per-frame]
                   [--plot-per-group] [--group-by GROUP_BY]
                   filename [filename ...]

Lima saving statistics log analyzer

positional arguments:
  filename             lima saving statistics log filename

optional arguments:
  -h, --help           show this help message and exit
  --print-stat         print statictics min/max/mean
  --plot-per-file      plot statistics per lima file
  --plot-per-frame     plot statistics per frame
  --plot-per-group     plot write time per group of log files
  --group-by GROUP_BY  number of files per plot group
```

**EXAMPLE**

[Plot per group](examples/ex_group.png)

[Plot per file](examples/ex_file.png)

[Plot per frame](examples/ex_frame.png)
